import React from "react";

/**
 * Represents the Partners of the Company
 */
class Partners extends React.Component {
  render() {
    return (
      <section id="partners" className="brand-logos">
        <h1 className="sec-heading">Our Partners</h1>
        <div>
          {/* Maps the properties of object brand in App.js */}
          {this.props.brands.map((data) => {
            return (
              <a href="/" key={data.link}>
                <img
                  src={data.link}
                  alt={data.alter}
                  key={data.alter}
                  title={data.title}
                />
              </a>
            );
          })}
        </div>
      </section>
    );
  }
}

export default Partners;
