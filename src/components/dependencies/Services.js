import React from "react";

/**
 * Represents the Services section of Company
 *
 */
class Services extends React.Component {
  render() {
    return (
      <section id="services">
        {/* Header */}
        <h1 className="sec-heading">Our Services</h1>
        {/* Maps the icons object properties from App.js using props  */}
        <ul>
          {this.props.icons.map((data) => {
            return (
              <li key={data.title}>
                <div>
                  <a href="/" key={data.title}>
                    <i className={data.class}></i>
                    <span>{data.title}</span>
                  </a>
                </div>
              </li>
            );
          })}
        </ul>

        <div id="service-footer">
          <a href="/" className="brand-btn">
            View All Service
          </a>
        </div>
      </section>
    );
  }
}

export default Services;
