import React from "react";
/**
 * Represents the recognition of top most companies
 */
class TopList extends React.Component {
  render() {
    return (
      <div>
        <section id="topList" className="brand-logos">
          {/* Section Heading */}
          <h1 className="sec-heading">
            Recognition as Top Mobile Development Company
          </h1>
          <div>
            {/* Maps the properties of object images in App.js */}
            {this.props.images.map((data) => {
              return (
                <a href="/" key={data.source}>
                  <img
                    src={data.source}
                    alt="Top 10 MobleApp Development Companies"
                    title="Top 10 MobleApp Development Companies"
                  />
                  <span>
                    Recognised Among Top 10 MobleApp Development Companies
                  </span>
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}
export default TopList;
