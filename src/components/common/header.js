import React from "react";

/**
 * Represents the Navbar Component
 */
class Navbar extends React.Component {
  render() {
    return (
      <>
        <header id="topHeader">
          {/* Mail and Contact */}
          <ul id="topInfo">
            <li>+974 98765432</li>
            <li>info@itecnology.com</li>
          </ul>
          <nav>
            <span className="logo">iTechnology</span>
            {/* Toggle Button */}
            <div className="menu-btn-3" onClick={this.menuBtnFunction}>
              <span></span>
            </div>
            {/* Navbar items */}
            <div className="mainMenu">
              <a href="/">
                <span>Technology</span>
              </a>
              <a href="/">
                <span>Service</span>
              </a>
              <a href="/">
                <span>Portfolio</span>
              </a>
              <a href="/">
                <span>About Us</span>
              </a>
              <a href="/">
                <span>Career</span>
              </a>
              <a href="/">
                <span>Blog</span>
              </a>
              <a href="/">Work With Us</a>
            </div>
          </nav>
        </header>
      </>
    );
  }
}

export default Navbar;
