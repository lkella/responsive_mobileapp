import React from "react";

/**
 * Represents Footer Section
 */
class Footer extends React.Component {
  render() {
    return (
      <>
        <footer>
          <div>
            <span className="logo">iTechnology</span>
          </div>
          <div className="row">
            {/* extract the  key-value pairs from the props object. */}
            {Object.entries(this.props.data).map(([key, value]) => {
              return (
                <div className="col-3" key={key}>
                  <span className="footer-cat">{key}</span>
                  <ul className="footer-cat-links">
                    {value.map((col) => {
                      return (
                        <li key={col}>
                          <a href="/">
                            <span>{col}</span>
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              );
            })}

            <div className="col-3" id="newsletter">
              <span className="footer-cat">Stay Connected</span>
              <form id="subscribe">
                <input
                  type="email"
                  id="subscriber-email"
                  placeholder="Enter Email Address"
                />
                <input type="submit" value="Subscribe" id="btn-scribe" />
              </form>

              <div id="address">
                <span className="footer-cat">Office Location</span>
                <ul>
                  <li>
                    <i className="far fa-building"></i>
                    <div>
                      Los Angeles
                      <br />
                      Office 9B, Sky High Tower, New A Ring Road, Los Angeles
                    </div>
                  </li>
                  <li>
                    <i className="fas fa-gopuram"></i>
                    <div>
                      Delhi
                      <br />
                      Office 150B, Behind Sana Gate Char Bhuja Tower, Station
                      Road, Delhi
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div id="copyright">&copy; All Rights Reserved 2019-2020</div>
          <div id="owner">
            <span>
              Designed by
              <a href="https://www.codingtuting.com">CodingTuting.Com</a>
            </span>
          </div>
          <a href="#topHeader" id="gotop">
            Top
          </a>
        </footer>
      </>
    );
  }
}

export default Footer;
