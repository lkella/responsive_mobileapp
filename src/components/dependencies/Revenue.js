import React from "react";

/**
 * Represents the Revenue for best companies section
 */
class Revenue extends React.Component {
  render() {
    return (
      <section id="revenue" className="brand-logos">
        <h1 className="sec-heading">
          We Drive Growth & Revenue for the Best Companies
        </h1>
        <div>
          {/* Maps the rev_brands object properties in App.js */}
          {this.props.rev_brands.map((data) => {
            return (
              <a href="/" key={data.source}>
                <img src={data.source} alt={data.alter} title={data.title} />
              </a>
            );
          })}
        </div>
      </section>
    );
  }
}

export default Revenue;
