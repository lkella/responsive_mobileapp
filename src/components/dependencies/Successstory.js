import React from "react";

/**
 * Represents Success Stories
 */
class SuccessStory extends React.Component {
  render() {
    return (
      <section id="success-story">
        {/* Section Heading */}
        <h1 className="sec-heading">Our Success Stories</h1>

        <div className="slider">
          {/* Left content */}
          <div className="col-6 slide-text">
            <div>
              <h2>World Travel Protection</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text.
              </p>
              <a href="/" className="brand-btn">
                Contact Us
              </a>
            </div>
          </div>

          {/* Right Content - Image */}
          <div className="col-6 slide-img">
            <img
              src="https://www.dropbox.com/s/ipx91osglyczpdt/delivery_experience.svg?raw=1"
              alt="World Travel App Development"
              title="World Travel Protection"
            />
          </div>
        </div>
      </section>
    );
  }
}

export default SuccessStory;
