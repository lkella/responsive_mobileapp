import React from "react";

import "./main.css";

import Navbar from "./components/common/header";
import Footer from "./components/common/footer";
import Intro from "./components/dependencies/intro";
import Experience from "./components/dependencies/delivery";
import Services from "./components/dependencies/services";
import SuccessStory from "./components/dependencies/successstory";
import Revenue from "./components/dependencies/revenue";
import Highlights from "./components/dependencies/highlights";
import Partners from "./components/dependencies/partners";
import TopList from "./components/dependencies/toplist";

/**
 * Object for Service Component
 * Passing class,title object properties
 */
var icons = [
  { class: "fas fa-laptop", title: "Stratagy and Consultant" },
  { class: "fas fa-users", title: "User Experience Design" },
  { class: "fas fa-mobile-alt", title: "Mobile App Development" },
  { class: "fab fa-chrome", title: "Web App Development" },
  { class: "fas fa-ribbon", title: "Quality Analysis and Testing" },
  { class: "fas fa-ticket-alt", title: "Application Management & Support" },
];

/**
 * Object for Revenue Component
 * Passing source,alter,title object properties
 */
var rev_brands = [
  {
    source: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
    alter: "Allianz",
    title: "Work with Allianz",
  },
  {
    source: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
    alter: "Audi",
    title: "Work with Audi",
  },
  {
    source: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
    alter: "BMW",
    title: "Work with BMW",
  },
  {
    source: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
    alter: "ESPN",
    title: "Work with ESPN",
  },
  {
    source: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
    alter: "LG",
    title: "Work with LG",
  },
  {
    source: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
    alter: "Nike",
    title: "Work with Nike",
  },
  {
    source: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
    alter: "Suzuki",
    title: "Work with Suzuki",
  },
  {
    source: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
    alter: "Visa",
    title: "Work with Visa",
  },
];

/**
 * Object  for Partners Component
 * Passing down link,alter,title object properties
 */
var brands = [
  {
    link: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
    alter: "Work with AWS",
    title: "Our Work",
  },
  {
    link: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
    alter: "Dell",
    title: "Work with Dell",
  },
  {
    link: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
    alter: "intel",
    title: "Work with intell",
  },
  {
    link: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
    alter: "IBM",
    title: "Work with IBM",
  },
  {
    link: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
    alter: "Microsoft",
    title: "Work with Microsoft",
  },
  {
    link: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
    alter: "Nasscom",
    title: "Work with Nasscom",
  },
  {
    link: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
    alter: "Samsung",
    title: "Work with Samsung",
  },
  {
    link: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
    alter: "Nvidia",
    title: "Work with Nvidia",
  },
];

/**
 * Object for TopList Component
 * Passing source as object property
 */
var images = [
  { source: "https://www.dropbox.com/s/19czj59oq0orbfa/tm.png?raw=1" },
  { source: "https://www.dropbox.com/s/130734rofy1f261/tata.png?raw=1" },
  {
    source: "https://www.dropbox.com/s/k17kwv9hiu9w98d/Infosys_logo.png?raw=1",
  },
  { source: "https://www.dropbox.com/s/mm4cnforc4pvwac/Wipro_Logo.png?raw=1" },
  { source: "https://www.dropbox.com/s/n4scpig8b3tfqkq/Amazon_logo.svg?raw=1" },
];

/**
 * Object for Footer Component
 */
var data = {
  Solution: [
    "Interprise App Development",
    "Android App Development",
    "ios App Development",
  ],
  Industries: ["Healthcare", "Sports", "ECommerce", "Construction", "Club"],
  "Quick Links": ["Reviews", "Terms & Condition", "Disclaimer", "Site Map"],
};

/**
 * Represents the Main Class Component
 */
class App extends React.Component {
  render() {
    return (
      <>
        <Navbar />
        <Intro />
        <Experience />
        <Services icons={icons} />
        <SuccessStory />
        <Revenue rev_brands={rev_brands} />
        <Highlights />
        <Partners brands={brands} />
        <TopList images={images} />
        <Footer data={data} />
      </>
    );
  }
}

//will be exposed globally
export default App;
